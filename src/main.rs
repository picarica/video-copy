use std::env;
use std::fs;
use std::path::PathBuf;
use std::process::Command;
use std::str::from_utf8;
use indicatif::{HumanDuration, ProgressBar, ProgressStyle, ProgressState};
use console::{style, Emoji};
use std::time::{Instant};
use std::{fmt::Write};

fn main() {
    static LOOKING_GLASS: Emoji<'_, '_> = Emoji("🔍  ", "");
    static TRUCK: Emoji<'_, '_> = Emoji("🚚  ", "");
    static SPARKLE: Emoji<'_, '_> = Emoji("✨ ", ":-)");
    let started = Instant::now();

    // Get command line arguments
    let args: Vec<String> = env::args().collect();
    if args.len() < 4 {
        println!("Usage: /video-copy <source_dir> <destination_dir> <min_length>");
        return;
    }

    let src_dir = PathBuf::from(&args[1]);
    let dst_dir = PathBuf::from(&args[2]);
    let min_length = args[3].parse::<f32>().unwrap();

    // Recursively search the source directory for video files
    let mut video_files = vec![];
    find_videos(&src_dir, &mut video_files);

    let mut all_files = 0;

    println!(
        "{} {}Counting files...",
        style("[1/2]").bold().dim(),
        LOOKING_GLASS,
    );
    //count the files
    for video_path in &video_files {
        let length = get_video_length(&video_path);
        if length > min_length {
            all_files += 1;
        }
    }

    println!(
        "{} {}Found {} files, starting copy ...",
        style("[2/2]").bold().dim(),
        TRUCK,
        all_files
    );
    //let all_files_u64 = all_files.to_string().parse::<u64>().unwrap();
    let pb = ProgressBar::new(all_files);

    // Move video files longer than min_length to destination directory
    for video_path in video_files {
        let length = get_video_length(&video_path);
        if length > min_length {
            let file_name = video_path.file_stem().unwrap().to_string_lossy().into_owned();
            let dst_path = dst_dir.join(&file_name);
            let dst_path_with_ext = dst_path.with_extension(video_path.extension().unwrap());
            fs::rename(&video_path, &dst_path_with_ext).expect("Unable to move file");
            pb.set_style(ProgressStyle::with_template("{spinner:.green} [{elapsed_precise}] [{wide_bar:.cyan/blue}] {pos}/{len} {percent}% ")
            .unwrap()
            .with_key("eta", |state: &ProgressState, w: &mut dyn Write| write!(w, "{:.1}s", state.eta().as_secs_f64()).unwrap())
            .progress_chars("#>-"));
            pb.inc(1);

        }
    }
    println!("{} Done in {}", SPARKLE, HumanDuration(started.elapsed()));
}

fn find_videos(dir: &PathBuf, video_files: &mut Vec<PathBuf>) {
    for entry in fs::read_dir(dir).unwrap() {
        let path = entry.unwrap().path();
        if path.is_dir() {
            find_videos(&path, video_files);
        } else {
            if let Some(extension) = path.extension() {
                if extension == "mp4" || extension == "avi" || extension == "m4v" || extension == "webm" {
                    video_files.push(path);
                }
            }
        }
    }
}

fn get_video_length(path: &PathBuf) -> f32 {
    let output = Command::new("ffprobe")
        .arg("-v")
        .arg("error")
        .arg("-select_streams")
        .arg("v:0")
        .arg("-show_entries")
        .arg("stream=duration")
        .arg("-of")
        .arg("default=noprint_wrappers=1:nokey=1")
        .arg(path)
        .output()
        .expect("Failed to execute command");

    let length_str = from_utf8(&output.stdout).unwrap().trim();
    length_str.parse::<f32>().unwrap()
}
